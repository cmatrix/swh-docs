.. _roadmaps:

Current and past roadmaps
-------------------------

.. toctree::
   :maxdepth: 2
   :titlesonly:

   roadmap-2022
   roadmap-2021
